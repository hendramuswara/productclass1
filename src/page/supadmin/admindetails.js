import React, { Component } from 'react'
import { Text, View, ImageBackground, Image, ToastAndroid, Alert } from 'react-native'
import { Card, Title, Button, Item, Container, Header, CardItem, Fab, Left, Icon, Body, Input, Content, Thumbnail, Right } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../../redux/action/AuthAction'


const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: token => dispatch(auth(token))
    }
}
class admindetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            username: '',
            id: ''
        }
    }

    componentDidMount() {
        this.getKonten()
    }

    getKonten = async () => {
        const { navigation } = this.props;
        const id = await navigation.getParam('id', '');
        try {
            const getData = async () => await Axios.get(
                `https://asuransi-glints-academy.herokuapp.com/api/user/selectAdmin/${id}`, {

                headers: {
                    'authorization': `${this.props.auth.usr}`
                }
            })
            getData()
                .then(res => {
                    // this.setState({
                    //     title: res.data.content.title,
                    //     body: res.data.content.body
                    // })
                    console.log(res)
                })
                .catch(err => {
                    console.log(err)
                })
        }
        catch (e) {
            console.log(err)
        }
    }


    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#f85f73' }}>

                    <Button transparent onPress={() => this.props.navigation.navigate('Admindata')}>
                        <Icon type='Ionicons' name='ios-arrow-back'></Icon>
                    </Button>

                    <Body>
                        <Title>{this.props.navigation.getParam('aemail')}</Title>
                    </Body>

                </Header>
                <Card >
                    <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection:'row', marginHorizontal: 20, marginTop: 10 }}>
                        <Text>Email</Text>
                        <Text>{this.props.navigation.getParam('aemail')}</Text>
                    </View>
                    <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection:'row' , marginHorizontal: 20, marginTop: 10}}>
                        <Text>Username</Text>
                        <Text>{this.props.navigation.getParam('ausername')}</Text>
                    </View>
                    <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection:'row' , marginHorizontal: 20, marginTop: 10}}>
                        <Text>Name</Text>
                        <Text>{this.props.navigation.getParam('aname')}</Text>
                    </View>
                    <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection:'row' , marginHorizontal: 20, marginTop: 10}}>
                        <Text>Phone</Text>
                        <Text>{this.props.navigation.getParam('aphone')}</Text>
                    </View>
                    <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection:'row', marginHorizontal: 20, marginTop: 10 }}>
                        <Text>Address</Text>
                        <Text>{this.props.navigation.getParam('aaddress')}</Text>
                    </View>
                    <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection:'row', marginHorizontal: 20, marginTop: 10 }}>
                        <Text>Gender</Text>
                        <Text>{this.props.navigation.getParam('agender')}</Text>
                    </View>
                    <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection:'row' , marginHorizontal: 20, marginTop: 10, Left}}>

                        <Text>Birth Place</Text>
                        <Text>{this.props.navigation.getParam('abp')}</Text>
                    </View>

                </Card>

            </Container>
        )
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(admindetails));
