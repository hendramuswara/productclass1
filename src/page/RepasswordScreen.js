import React, { Component } from 'react'
import { Text, View, ImageBackground, TouchableOpacity, ToastAndroid } from 'react-native'
import { Card, Button, Item, Container, CardItem, Body, Input, Content } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction'


class login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            token: '',
            password:'',
            passwords: ''
        }
    }

    forgot = async () => {
        send = async (objParam) => await Axios.put(
            `https://asuransi-glints-academy.herokuapp.com/api/user/reset`,
            objParam
        )

        send({
            password: this.state.password,
            token: this.state.token
        })
            .then(res => {
                this.props.navigation.navigate('Login')
                console.log('Berhasil!')
            })
            .catch(err => {
                ToastAndroid.show('Gagal', ToastAndroid.SHORT)
                console.log(this.state.password)
                console.log(this.state.token)
                console.log(err.message)
            })
    }

    render() {
        return (
            <KeyboardAwareScrollView>
                <Container>
                    <ImageBackground source={require('../img/ganteng2.png')} style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                        <Card transparent style={{ width: '80%', height: '65%', justifyContent: 'center', backgroundColor: 'rgba(255,255,255,0)' }}>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 20, color: '#f85f73', alignSelf: 'center', fontWeight: 'bold' }}>Forget Password</Text>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="Token"
                                            onChangeText={(text) => this.setState({ token: text })}
                                            style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>


                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="New Password"
                                            onChangeText={(text) => this.setState({ password: text })}
                                            style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>


                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="Confirm New Password"
                                            onChangeText={(text) => this.setState({ passwords: text })}
                                            style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Button regular
                                     onPress={
                                        () => {
                                            if (this.state.password === this.state.passwords) {
                                                this.forgot()
                                            } else {
                                                ToastAndroid.show('Password belum sama!', ToastAndroid.SHORT)
                                            }
                                        }
                                    }
                                    style={{ backgroundColor: '#f85f73', width: '50%', justifyContent: 'center' }}>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#fbe8d3' }}>Submit</Text>
                                </Button>
                            </CardItem>

                        </Card>
                    </ImageBackground>
                </Container>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: id => dispatch(auth(id))
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(login));