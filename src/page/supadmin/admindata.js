import React, { Component } from 'react'
import { Text, View, ImageBackground, Image, ToastAndroid,Alert } from 'react-native'
import { Card, Title,Button, Item, Container, Header, CardItem, Fab, Icon, Body, Input, Content, Thumbnail, Right } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../../redux/action/AuthAction'


const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: token => dispatch(auth(token))
    }
}

class admindata extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            name: '',
            email: '',
            username: '',
            password: '',
            _id: ''
        }
    }

    componentDidMount() {
        this.dataAdmin()
        setInterval(this.dataAdmin, 5000)
    }

    dataAdmin = async () => {
        try {
            const prof = async () => await Axios.get(
                'https://asuransi-glints-academy.herokuapp.com/api/user/showAdmin',

                {
                    headers: {
                        'authorization': `${this.props.auth.usr}`
                    }
                }
            )
            prof()
                .then(res => {
                    this.setState({
                        data: res.data.result
                    })
                    console.log(res.data.result)
                })
                .catch(e => {
                    console.log(e)
                })
        } catch (e) {
            console.log(e)
        }
    }

    deleteAdmin = async (id) => {
        const tokens = await AsyncStorage.getItem('@token')
        try {
            const del = async () => await Axios.delete(
                `https://asuransi-glints-academy.herokuapp.com/api/user/deleteAdmin/${id}`,

                {
                    headers: {
                        'Authorization': `${this.props.auth.usr}`
                    }
                }
            )
            del()
                .then(res => {
                    // this.setState({
                    //     data: res.data
                    // })
                    console.log(res)
                    ToastAndroid.show('Data Deleted', ToastAndroid.SHORT)
                })
                .catch(e => {
                    console.log(e)
                })
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        const loops = this.state.data.map(value => {
            // console.log(value)
            return (
                <Card key={value._id} style={{ marginTop: '1%' }}>
                    <CardItem style={{ backgroundColor: '#ffff' }} 
                        button onPress={() => this.props.navigation.navigate('Admindetail',{
                            ausername : value.username,
                            aname : value.name,
                            aemail : value.email,
                            aphone : value.phone,
                            aaddress: value.address,
                            agender: value.gender,
                            abp : value.birthPlace,
                           
                        })}>
                        <Thumbnail source={{ uri: `${value.image}` }} />
                        <Text>{value.email}</Text>
                        <Right>
                            <Button transparent  onPress={() => {

                                Alert.alert(
                                    `Delete ${value.username}`,
                                    'Are you sure want to delete this product ?', [
                                    { text: 'NO', onPress: () => this.props.navigation.navigate('Admindata') },
                                    { text: 'YES', onPress: () => this.deleteAdmin(value._id) },
                                ]
                                );

                            }

                            }>
                                <Icon name='trash' />
                            </Button>
                        </Right>
                    </CardItem>

                </Card>
            )
        })
        return (

            <KeyboardAwareScrollView>
                <Container>
                    <Header style={{ backgroundColor: '#f85f73' }}>

                        <Button transparent onPress={() => this.props.navigation.navigate('Supahome')}>
                            <Icon type='Ionicons' name='ios-arrow-back'></Icon>
                        </Button>

                        <Body>
                            <Title>Data Admin</Title>
                        </Body>

                    </Header>

                    {loops}

                    <Fab position="bottomRight" style={{ backgroundColor: '#F62F5E' }} onPress={() => this.props.navigation.navigate('Adminadd')}>
                        <Icon name="add" />

                    </Fab>

                </Container>
            </KeyboardAwareScrollView>

        )
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(admindata));