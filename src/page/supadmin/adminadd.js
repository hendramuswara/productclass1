import React, { Component } from 'react'
import { Text, View, ToastAndroid, Image } from 'react-native'
import { Input, Title, Body, Right, Textarea, Header, Container, Content, Fab, Card, CardItem, Button, Item, Left, CheckBox, Icon, Tabs, Tab, Thumbnail } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../../redux/action/AuthAction'
import { withNavigation } from 'react-navigation'


const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: token => dispatch(auth(token))
    }
}
class adminadd extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            username: '',
            password: '',
        }
    }

    register = async => {
        try {
            console.log('Kepencet')
            const regis = async (objparam) => await Axios.post(
                'https://asuransi-glints-academy.herokuapp.com/api/user/admin',
                objparam,

                {
                    headers: {
                        'authorization': `${this.props.auth.usr}`
                    }
                }
            )

            regis({
                'name': this.state.name,
                'username': this.state.username,
                'email': this.state.email,
                'password': this.state.password,

            })
                .then(res => {
                    console.log(res)
                    this.setState({
                        name: null,
                        email: null,
                        username: null,
                        password: null
                    })
                    ToastAndroid.show('Berhasil Daftar!', ToastAndroid.SHORT)
                    this.props.navigation.navigate('Admindata')
                })
                .catch(e => {
                    console.log(e)
                   
                    ToastAndroid.show('Daftar Gagal!', ToastAndroid.SHORT)
                    this.props.navigation.navigate('Adminadd')
                })
        }
        catch (err) {
            console.log(err)
        }
    }

    render() {
        return (
            <KeyboardAwareScrollView>

                <Container>
                    <Header style={{ backgroundColor: '#f85f73' }}>

                        <Button transparent onPress={() => this.props.navigation.navigate('Admindata')}>
                            <Icon type='Ionicons' name='ios-arrow-back'></Icon>
                        </Button>

                        <Body>
                            <Title>Add Admin</Title>
                        </Body>

                    </Header>


                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{

                            borderColor: '#F62F5E', width: '80%', alignSelf: 'center',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }}>
                            <Input placeholder="Name"
                                onChangeText={(text) => this.setState({ name: text })}
                                style={{ color: '#283c63', width: '80%' }} />
                        </Item>
                    </View>
                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{
                            borderColor: '#F62F5E', width: '80%', alignSelf: 'center',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }}>
                            <Input placeholder="Username"
                                onChangeText={(text) => this.setState({ username: text })}
                                style={{ color: '#283c63', width: '80%' }} />
                        </Item>

                    </View>
                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{
                            borderColor: '#F62F5E', width: '80%', alignSelf: 'center',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }}>
                            <Input placeholder="Email"
                                onChangeText={(text) => this.setState({ email: text })}
                                style={{ color: '#283c63', width: '80%' }} />
                        </Item>
                    </View>
                    <View style={{ marginTop: '5%' }}>

                        <Item regular style={{

                            borderColor: '#F62F5E', width: '80%', alignSelf: 'center',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }}>
                            <Input secureTextEntry={true} placeholder="Password"
                                onChangeText={(text) => this.setState({ password: text })}
                                style={{ width: '80%', color: '#283c63', width: '80%' }} />
                        </Item>
                    </View>
                    <View style={{ marginTop: '5%' }}>

                        <Button regular
                            onPress={() => this.register()}
                            style={{ backgroundColor: '#f85f73', width: '50%', justifyContent: 'center', alignSelf: 'center' }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#fbe8d3' }}>Add Admin</Text>
                        </Button>
                    </View>
                </Container>


            </KeyboardAwareScrollView>

        )
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(adminadd))