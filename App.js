import React, { Component } from 'react'
import { createStackNavigator } from 'react-navigation-stack';
import {
  createAppContainer,
} from 'react-navigation'

import { Icon } from 'native-base'
import Register from './src/page/register'
import Login from './src/page/login'
import Sahome from './src/page/sadmin/sahome'
import Uhome from './src/page/user/Uhome'
import Supahome from './src/page/supadmin/supahome'
import Admindata from './src/page/supadmin/admindata'
import Adminadd from './src/page/supadmin/adminadd'
import Admindetail from './src/page/supadmin/admindetails'
import Saprofile from './src/page/sadmin/saprofile'
import Editadmin from './src/page/sadmin/adminedit'
import UhomeScreen from './src/page/user/home'
import DetailScreen from './src/page/user/Details'
import ForgetScreen from './src/page/ForgetScreen'
import RepasswordScreen from './src/page/RepasswordScreen'
// import ForgetScreen from './src/pages/ForgetScreen'
// import SplashScreen from './src/pages/Splash'
// import RepasswordScreen from './src/pages/RepasswordScreen'


import { Provider } from 'react-redux';
import configureStore from './store';
const store = configureStore();


const Apps = createStackNavigator(
  {
    Login: Login,
    Register: Register ,
    Sahome : Sahome,
    Supahome :  Supahome,
    Uhome :  Uhome ,
    Admindata:  Admindata ,
    Adminadd:  Adminadd,
    Admindetail:  Admindetail,
    Saprofile :  Saprofile,
    Editadmin :  Editadmin,
    Details : DetailScreen,
    Uhomes : UhomeScreen,
    Forget : ForgetScreen,
    Repassword: RepasswordScreen
  },
  {

    initialRouteName: 'Login',
    headerMode: 'none',
  });


const AppContainer = createAppContainer(Apps)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>

    )
  }
}
