import React, { Component } from 'react'
import { Text, View, ImageBackground, Image, ToastAndroid } from 'react-native'
import { Card, Button, Item, Container, Header, CardItem, Body, Input, Content, Thumbnail } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../../redux/action/AuthAction'

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: token => dispatch(auth(token))
    }
}

class sahome extends Component {
    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#f85f73' }}>
                    <Body>
                        <Text style={{ color: '#fbe8d3', fontSize: 18 }}>Admin Page</Text>
                    </Body>
                </Header>

                <View style={{ display: 'flex', justifyContent: 'space-evenly', flexDirection: 'row', paddingTop: 15 }}>
                    <Card style={{ width: '40%', justifyContent: 'center' }}>
                        <CardItem>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Thumbnail source={require('../../img/education.png')}
                                    style={{ width: '70%' }} />
                                <Text style={{ alignSelf: 'center' }}>Insurance</Text>
                            </Body>
                        </CardItem>
                    </Card>
                    <Card style={{ width: '40%', justifyContent: 'center' }} >
                        <CardItem style={{ alignSelf: 'center' }}>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Thumbnail source={require('../../img/promod.png')} style={{ width: '70%' }} />
                                <Text style={{ alignSelf: 'center' }}>Promo</Text>
                            </Body>
                        </CardItem>
                    </Card>
                </View>

                <View style={{ display: 'flex', justifyContent: 'space-evenly', flexDirection: 'row', paddingTop: 15 }}>
                    <Card style={{ width: '40%' }}>
                        <CardItem style={{ alignSelf: 'center' }}>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Thumbnail source={require('../../img/kamus.png')} style={{ width: '70%' }} />
                                <Text style={{ alignSelf: 'center' }}>Pembayaran</Text>
                            </Body>
                        </CardItem>
                    </Card>
                    <Card style={{ width: '40%' }}>
                        <CardItem style={{ alignSelf: 'center' }} button onPress={() => this.props.navigation.navigate('Saprofile')}>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Thumbnail source={require('../../img/user.png')} style={{ width: '70%' }} />
                                <Text style={{ alignSelf: 'center' }}>Profile</Text>
                            </Body>

                        </CardItem>
                    </Card>
                </View>

                <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', paddingTop: 15 }}>
                    <Card style={{ width: '40%' }}>
                        <CardItem style={{ alignSelf: 'center' }} button onPress={() =>
                         
                         this.props.navigation.navigate('Login')}>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Thumbnail source={require('../../img/logout.png')} style={{ width: '70%' }} />
                            <Text  style={{ alignSelf: 'center' }}>Logout</Text>
                            </Body>
                        </CardItem>
                    </Card>
                </View>
            </Container>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(sahome));