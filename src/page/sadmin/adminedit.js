import React, { Component } from 'react'
import { Text, View, ImageBackground, Image, ToastAndroid } from 'react-native'
import { Card,Left, Button, Item, Container, Header, CardItem,Body, Input, Title, Icon,Thumbnail } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../../redux/action/AuthAction'

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: token => dispatch(auth(token))
    }
}
 class adminedit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            name: '',
            username: '',
            phone: '',
            address: '',
            email:'',
            birthPlace:'',
            _id: '',
            gbr:''
        }
    }

    componentDidMount() {
        this.detailData()
    }

    detailData = async () => {
        const tokens = await AsyncStorage.getItem('@token')
        try {
            const det = async () => await Axios.get(
                `https://asuransi-glints-academy.herokuapp.com/api/user/show`,
                {
                    headers: {
                        'Authorization': `${this.props.auth.usr}`
                    }
                }
            )
            det()
                .then(res => {
                    this.setState({
                        name: res.data.result.name,
                        username : res.data.result.username,
                        phone : res.data.result.phone,
                        address: res.data.result.address,
                        birthPlace: res.data.result.birthPlace

                    })
                    console.log(res.data.result)
                })
                .catch(e => {
                    console.log(e)
                })
        } catch (e) {
            console.log(e)
        }
    }

    editData = async () => {
        try {
            const edits = async (obj) => await Axios.put(
                `https://asuransi-glints-academy.herokuapp.com/api/user/update`,obj,

                {
                    headers: {
                        'Authorization': `${this.props.auth.usr}`
                    }
                }
            )

            edits({
                name: this.state.name,
                username : this.state.username,
                phone : this.state.phone,
                address: this.state.address,
                birthPlace: this.state.birthPlace

            })
            .then(res => {
                console.log(res)
               
                ToastAndroid.show('Berhasil Update!', ToastAndroid.SHORT)
            })
            .catch(e => {
                console.log(e)
                ToastAndroid.show('Update Gagal!', ToastAndroid.SHORT)
            })
        }
        catch(err){
            console.log(err)
        }
    }

    render() {
        return (
            <KeyboardAwareScrollView>
            <Container style={{ justifyContent: 'center' }}>
                <Header style={{ backgroundColor: '#F62F5E' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate('Main')}>
                            <Icon type='Ionicons' name='ios-arrow-back'></Icon>
                        </Button>
                    </Left>
                    <Body>
                        <Title >Edit Admin</Title>
                    </Body>

                </Header>
                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{
                            width: '80%', alignSelf: 'center', borderColor: '#F62F5E',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }} >
                            <Input value={this.state.name} placeholder="Name" onChangeText={(text) => this.setState({ name: text })} />
                        </Item>
                    </View>

                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{
                            width: '80%', alignSelf: 'center', borderColor: '#F62F5E',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }} >
                            <Input value={this.state.username} placeholder="Username" onChangeText={(text) => this.setState({ username: text })} />
                        </Item>
                    </View>
                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{
                            width: '80%', alignSelf: 'center', borderColor: '#F62F5E',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }} >
                            <Input value={this.state.phone} placeholder="Phone" onChangeText={(text) => this.setState({ phone: text })} />
                        </Item>
                    </View>
                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{
                            width: '80%', alignSelf: 'center', borderColor: '#F62F5E',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }} >
                            <Input value={this.state.address} placeholder="Address" onChangeText={(text) => this.setState({ address: text })} />
                        </Item>
                    </View>
                    <View style={{ marginTop: '5%' }}>
                        <Item regular style={{
                            width: '80%', alignSelf: 'center', borderColor: '#F62F5E',
                            borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                        }} >
                            <Input value={this.state.birthPlace} placeholder="Place" onChangeText={(text) => this.setState({ birthPlace: text })} />
                        </Item>
                    </View>
           
                    <View style={{ marginTop: '5%' }}>
                        <Button style={{ width: '80%', alignSelf: 'center', justifyContent: 'center', backgroundColor: '#F62F5E' }}
                            onPress={() => {
                                this.editData()
                                this.props.navigation.navigate('Saprofile')}}>
                            <Text style={{ color: 'white', fontSize: 20, alignSelf: 'center' }}>Update</Text>
                        </Button>
                    </View>

              
            </Container> 
            </KeyboardAwareScrollView>


        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(adminedit));
