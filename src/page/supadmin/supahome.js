import React, { Component } from 'react'
import { Text, View, ImageBackground, Image, ToastAndroid } from 'react-native'
import { Card, Button, Item, Container, Header, CardItem, Body, Input, Content, Thumbnail } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../../redux/action/AuthAction'

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: token => dispatch(auth(token))
    }
}


class supahome extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: ''
        }
    }

    componentDidMount() {
        this.dataProfil()
    }

    dataProfil = async () => {
        const tokens = await AsyncStorage.getItem('@token')
        try {
            const prof = async () => await Axios.get(
                `https://asuransi-glints-academy.herokuapp.com/api/user/show`,
                {
                    headers: {
                        'authorization': `${this.props.auth.usr}`
                    }
                }
            )
            console.log(tokens)
            prof()
                .then(res => {
                    this.setState({
                        data: res.data.result
                    })
                    console.log(this.state.data)
                 console.log(res.data.result)
                })
                .catch(e => {
                    console.log(e)
                })
        } catch (e) {
            console.log(e)
        }
    }
    render() {
        return (
            <Container>
                
                {/* <Header style={{ backgroundColor: '#f85f73' }}>
                    <Body>
                        <Text style={{ color: '#fbe8d3', fontSize: 18 }}>Super Admin Page</Text>
                    </Body>
                </Header> */}

                <View style={{ display: 'flex', justifyContent: 'center', 
                alignItems: 'center' }}>
                    
                     <Image source={{uri:  `${this.state.data.image}`}} style={{ marginTop: '3%' ,width: 100, height:100, borderRadius: 100/2 }} />
                    <Text style={{ fontSize: 20, alignSelf: 'center', marginTop: '1%' }}>{this.state.data.name}</Text>
                </View>




                <View style={{ display: 'flex', justifyContent: 'space-evenly', flexDirection: 'row', paddingTop: 15 }}>
                    <Card style={{ width: '40%' }}>
                        <CardItem style={{ alignSelf: 'center' }} button onPress={() => this.props.navigation.navigate('Admindata')}>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Thumbnail source={require('../../img/user.png')} style={{ width: '70%' }} />
                                <Text style={{ alignSelf: 'center' }}>Admin</Text>
                            </Body>

                        </CardItem>
                    </Card>
                    <Card style={{ width: '40%' }}>
                        <CardItem style={{ alignSelf: 'center' }}>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Thumbnail source={require('../../img/kamus.png')} style={{ width: '70%' }} />
                                <Text style={{ alignSelf: 'center' }}>Kamus</Text>
                            </Body>
                        </CardItem>
                    </Card>
                </View>
      
                <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', paddingTop: 15 }}>
                    <Card style={{ width: '40%' }}>
                        <CardItem style={{ alignSelf: 'center' }} button onPress={() =>
                         
                            this.props.navigation.navigate('Login')}>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Thumbnail source={require('../../img/logout.png')} style={{ width: '70%' }} />
                                <Text style={{ alignSelf: 'center' }}>Logout</Text>
                            </Body>
                        </CardItem>
                    </Card>
                </View>
         
            </Container>
        )
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(supahome));
