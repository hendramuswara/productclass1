import { combineReducers } from 'redux';
import auth from './AuthReducer';

const IndexReducer = combineReducers({
    auth: auth
});

export default IndexReducer;