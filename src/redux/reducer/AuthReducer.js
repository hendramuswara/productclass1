import { FETCH_USR } from '../type/AuthType';
const initialState = {
    usr: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USR:
            return {
                ...state,
                usr: action.payload
            };
        default:
            return state;
    }
};