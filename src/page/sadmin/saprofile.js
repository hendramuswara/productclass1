import React, { Component } from 'react'
import { Text, View, ImageBackground, Image, ToastAndroid } from 'react-native'
import { Card, Button, Item, Container, Header, CardItem, Body, Input, Title, Icon,Thumbnail } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../../redux/action/AuthAction'
import ImagePicker from 'react-native-image-picker'

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: token => dispatch(auth(token))
    }
}


class saprofile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: '',
            image: null
        }
    }


    componentDidMount() {
        setInterval(this.dataProfil, 2000)
    }


    selectGambar = () => {
        const options = {
            title: 'Select Image',
            storageOptions: {
                skinBackup: true
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            this.setState({
                image: response
            })

            if (response.didCancel) {
                console.log('Cancelled');
            } else if (response.error) {
                console.log('error', response.error);
            } else {
                this.setState({
                    image: response
                })
                this.foto()
                this.props.navigation.navigate('Saprofile')
            }
        });
    }

    foto = async () => {
        const tokens = await AsyncStorage.getItem('@token')
        try {
            console.log(this.state.image.fileName)
            let image = new FormData()
            image.append('image',
                {
                    uri: this.state.image.uri,
                    name: this.state.image.fileName,
                    type: this.state.image.type
                }
            )
            console.log(image)

            const addimage = async (objparam) => await Axios.put(
                `https://asuransi-glints-academy.herokuapp.com/api/user/upload`,
                objparam,
                {
                    headers: {
                        'Authorization': `${this.props.auth.usr}`
                    }
                }
            );
            addimage(image)
                .then(response => {
                    ToastAndroid.show('Update image Success', ToastAndroid.SHORT)
                    console.log(response)
                  

                })
                .catch(err => {
                    ToastAndroid.show('Update image Failed', ToastAndroid.SHORT)
                    console.log('errorbgst',err)
                })
        }
        catch (e) {
            console.log(e)
        }
    }


    dataProfil = async () => {
        const tokens = await AsyncStorage.getItem('@token')
        try {
            const prof = async () => await Axios.get(
                `https://asuransi-glints-academy.herokuapp.com/api/user/show`,
                {
                    headers: {
                        'authorization': `${this.props.auth.usr}`
                    }
                }
            )
          
            prof()
                .then(res => {
                    this.setState({
                        data: res.data.result
                    })
                    
                    console.log(this.state.data)
                    console.log(res.data.result)
                })
                .catch(e => {
                    console.log(e)
                })
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        return (
            <Container style={{ backgroundColor: 'black' }}>

                <Header style={{ backgroundColor: '#f85f73' }}>

                    <Button transparent onPress={() => this.props.navigation.navigate('Sahome')}>
                        <Icon type='Ionicons' name='ios-arrow-back'></Icon>
                    </Button>

                    <Body>
                        <Title>{this.state.data.username}</Title>
                    </Body>

                </Header>

                <View style={{ marginTop: 20, display: 'flex', justifyContent: 'center' }}>
                    <Button transparent  onPress={() => this.selectGambar()} style={{justifyContent:'center', marginTop:15}}>
                    <Image source={{ uri: `${this.state.data.image}` }}  
                    style={{ marginBottom: 10, width: 100, height: 100, borderRadius: 100 / 2, alignSelf: 'center'}} />
                    </Button>
                    
                    
                    <Text style={{ fontSize: 24, alignSelf: 'center', color: '#A2474E', fontWeight: 'bold',marginTop:15 }}>{this.state.data.name}</Text>
                    <Text style={{ fontSize: 15, alignSelf: 'center', color: '#ACA6A6',marginTop:15 }}>{this.state.data.birthPlace}</Text>
                </View>

                <View style={{ marginTop: 30, marginHorizontal: 10 }}>
                    <Card title="Local Modules">
                        <View>
                            <Text style={{ marginTop: 20, marginHorizontal: 20, fontSize: 20, color: '#EB8094' }}>Profile</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Role </Text>
                                <Text style={{ color: 'green' }}>{this.state.data.role} </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Email </Text>
                                <Text>{this.state.data.email} </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Address </Text>
                                <Text>{this.state.data.address} </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Phone </Text>
                                <Text>{this.state.data.phone} </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10, marginBottom: 30 }}>
                                <Button transparent onPress={() => this.props.navigation.navigate('Editadmin')}>
                                    <Text style={{ color: '#EB8094' }}>Edit Profile </Text>
                                </Button>
                            </View>
                        </View>
                    </Card>
                </View>
            </Container>

        )
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(saprofile));